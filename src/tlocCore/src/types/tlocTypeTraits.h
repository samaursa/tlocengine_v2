#ifndef _TLOC_CORE_TYPE_TRAITS_H_
#define _TLOC_CORE_TYPE_TRAITS_H_

#include <type_traits>

namespace tloc
{
  namespace type_traits
  {
    template <bool B>
    using bool_constant = std::integral_constant<bool, B>;

    template <typename T, typename... T_Rest>
    struct is_any : std::false_type { };

    template <typename T, typename T_First>
    struct is_any<T, T_First> : std::is_same<T, T_First> { };

    template <typename T, typename T_First, typename... T_Rest>
    struct is_any<T, T_First, T_Rest...>
      : bool_constant<std::is_same<T, T_First>::value || is_any<T, T_Rest...>::value>
    { };

  };
};

#endif
