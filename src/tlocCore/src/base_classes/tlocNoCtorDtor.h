#ifndef _TLOC_CORE_BASE_CLASSES_NO_CTOR_NO_DTOR_
#define _TLOC_CORE_BASE_CLASSES_NO_CTOR_NO_DTOR_

#include <tlocCore/tlocCoreBase.h>

namespace tloc { namespace core { namespace base_classes {

  class no_ctor_dtor
  {
  protected:
    no_ctor_dtor();
    no_ctor_dtor(const no_ctor_dtor& a_other);
    ~no_ctor_dtor();

  };

};};};

#endif